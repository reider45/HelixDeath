package me.reider45.HelixDeath.Utilities;

import java.util.HashMap;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import me.reider45.HelixDeath.HelixDeath;
import me.reider45.HelixDeath.Particles.ParticleEffect;

public class Launch {

	HelixDeath pl;

	public Launch(HelixDeath ms) {
		pl = ms;
		playerColors = new HashMap<Player, Color>();
	}

	/**
	 * Save player's trail colors
	 */
	public HashMap<Player, Color> playerColors;

	public void setColor(Player p, Color color) {
		playerColors.put(p, color);
	}

	public Color getColor(Player p) {
		if(playerColors.get(p) == null)
			setColor(p, Color.RED);
		return playerColors.get(p);
	}

	/**
	 * Launch a firework for a player
	 */
	public void launchFor(Player p) {
		Location loc = p.getLocation().clone();
		final Color color = getColor(p);
		System.out.println(color);

		Firework fw = (Firework) loc.getWorld().spawnEntity(loc.add(0, 1, 0), EntityType.FIREWORK);
		FireworkMeta meta = fw.getFireworkMeta();
		FireworkEffect effect = FireworkEffect.builder().flicker(true).trail(false).with(Type.BALL_LARGE)
				.withColor(color).withFade(Color.WHITE).build();
		meta.addEffect(effect);
		meta.setPower(2);
		fw.setFireworkMeta(meta);
		fw.setVelocity(fw.getVelocity().multiply(0.75));

		new BukkitRunnable() {

			int radius = 1;

			public void run() {

				double y = fw.getLocation().getY();
				double x = radius * Math.cos(y);
				double z = radius * Math.sin(y);

				Location toPlay = fw.getLocation().clone();
				toPlay.add(x, 0, z);

				for(int c = 0; c<2; c++)
					ParticleEffect.REDSTONE.display( new ParticleEffect.OrdinaryColor(color), toPlay.clone().add(0, c*0.2, 0), 50);

				double x2 = radius * (Math.cos(y) * -1);
				double z2 = radius * (Math.sin(y) * -1);

				Location toPlay2 = fw.getLocation().clone();
				toPlay2.add(x2, 0, z2);

				for(int c = 0; c<2; c++)
					ParticleEffect.REDSTONE.display( new ParticleEffect.OrdinaryColor(color), toPlay2.clone().add(0, c*0.2, 0), 50);

				if (fw.isDead())
					this.cancel();
			}
		}.runTaskTimer(pl, 0L, 0L);

	}

}
