package me.reider45.HelixDeath.Utilities;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class IFactory {
	
	public static ItemStack createTrailItem(ChatColor color, short data) {
		ItemStack is = new ItemStack(Material.INK_SACK, 1, data);
		ItemMeta meta = is.getItemMeta();
		meta.setDisplayName(color + "Firework Trail");
		is.setItemMeta(meta);
		return is;
	}

}
