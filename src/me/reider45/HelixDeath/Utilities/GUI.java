package me.reider45.HelixDeath.Utilities;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.reider45.HelixDeath.HelixDeath;

public class GUI implements Listener {
	
	HelixDeath pl;
	Inventory menu;
	
	public GUI(HelixDeath ms) {
		pl = ms;
		menu = Bukkit.createInventory(null, InventoryType.HOPPER, ChatColor.BOLD+"Trail Selector");
		
		menu.addItem(IFactory.createTrailItem(ChatColor.GOLD, (short)14));
		menu.addItem(IFactory.createTrailItem(ChatColor.RED, (short)1));
		menu.addItem(IFactory.createTrailItem(ChatColor.LIGHT_PURPLE, (short)5));
		Bukkit.getPluginManager().registerEvents(this, pl);
	}
	
	public void openMenu(Player p) {
		p.openInventory(menu);
	}
	
	@EventHandler
	public void onColorSelect(InventoryClickEvent e) {
		if(!e.getInventory().getTitle().equals(menu.getName())) return;
		
		ItemStack selected = e.getCurrentItem();
		if(selected == null) return;
		if(selected.getType() != Material.INK_SACK) return;
		
		byte color = (byte) selected.getDurability();
		Player p = (Player) e.getWhoClicked();
		if(color == 14){
			// Orange
			pl.getLaunch().setColor(p, Color.ORANGE);
			
		} else
		if(color == 1) {
			// Red
			pl.getLaunch().setColor(p, Color.RED);

		} else
		if(color == 5) {
			// Purple
			pl.getLaunch().setColor(p, Color.PURPLE);
		}
		
		p.closeInventory();
		
	}

}