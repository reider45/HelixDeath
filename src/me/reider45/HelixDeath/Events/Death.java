package me.reider45.HelixDeath.Events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import me.reider45.HelixDeath.HelixDeath;

public class Death implements Listener {
	
	HelixDeath pl;
	public Death(HelixDeath ms) {
		pl = ms;
		Bukkit.getPluginManager().registerEvents(this, pl);
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		if(e.getEntity().getKiller() instanceof Player)
			pl.getLaunch().launchFor(e.getEntity());
	}

}
