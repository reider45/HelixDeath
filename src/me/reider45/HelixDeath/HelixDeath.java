package me.reider45.HelixDeath;

import org.bukkit.plugin.java.JavaPlugin;

import me.reider45.HelixDeath.Command.CommandHandler;
import me.reider45.HelixDeath.Events.Death;
import me.reider45.HelixDeath.Utilities.GUI;
import me.reider45.HelixDeath.Utilities.Launch;

public class HelixDeath extends JavaPlugin {

	GUI gui;
	Launch launch;

	public void onEnable() {
		gui = new GUI(this);
		launch = new Launch(this);
		new Death(this);
		
		getCommand("color").setExecutor(new CommandHandler(this));
	}

	public GUI getGUI() {
		return gui;
	}

	public Launch getLaunch() {
		return launch;
	}

}
